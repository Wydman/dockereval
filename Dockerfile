FROM adoptopenjdk/maven-openjdk13 as build
WORKDIR /src
COPY pom.xml .
RUN mvn dependency:go-offline
COPY . /src
RUN mvn package
EXPOSE 8181
CMD java -jar /src/target/*.jar