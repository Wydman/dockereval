# Simple HTTP Hello World

Ce projet démarre un serveur HTTP basique qui répond aux requêtes HTTP sur le endpoint **/hello**.

Le serveur écoute sur le port **8181**. 

## Build du projet

Avec maven : `mvn package`. 

Le jar généré est disponible dans /target. Un jar auto-exécutable avec les dépendances du projet est disponible (sous le nom `*-jar-with-dependencies.jar`).

## Exemple

```
# Request
GET /hello

# Response
200 

Hello, world! It is currently 20:20:02.020
``` 


## Conteneurisation du projet

Lancer GitBash ou l'invité de commande windows depuis la racine du projet puis executer la commande suivante :

```
docker build -t docker-eval .
``` 

Une fois l'image créée, lancer l'invité de commande windows (CMD depuis la barre de recherche)
puis executer la commande suivante :

```
docker run -it -p 8181:8181 docker-eval
``` 

le containeur restera disponible sur le port 8181 du localhost.

